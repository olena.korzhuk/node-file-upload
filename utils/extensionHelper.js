const extensionRegex = /\.(txt|js|json|yaml|xml|log)$/;

const isValidExtension = (fileName) => {
    return extensionRegex.test(fileName);
};

const getExtension = (fileName) => {
    try {
        return extensionRegex.exec(fileName)[1];
    } catch (e) {
        return null
    }


};

module.exports = {
    isValidExtension,
    getExtension
}