const express = require('express');
const fs = require('fs');
const path = require('path');
const FilesController = require('./controllers/FilesController');
const morgan = require('morgan');
const app = express();
const PORT = 8080;

global.filesDir = path.join(path.resolve(__dirname), '/api/files');

fs.mkdirSync(path.join(__dirname, '/api/files'), {recursive: true})

app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(morgan('tiny'));

app.post('/api/files', FilesController.createFile);

app.get('/api/files/:filename', FilesController.getFile);

app.get('/api/files', FilesController.getFiles);

app.listen(PORT, () => {
    console.log(`Server is up and listening on port ${PORT}`);
});
