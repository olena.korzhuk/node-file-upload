const responseMessages = {
    NO_CONTENT_PARAM : `Please specify 'content' parameter`,
    NO_FILENAME_PARAM : `Please specify 'filename' parameter`,
    FILE_CREATED_SUCCESSFULLY: 'File created successfully',
    SERVER_ERROR: 'Server error',
    SUCCESS_MESSAGE: 'Success',
    INVALID_FILE_FORMAT: 'Invalid file format',
    FILE_ALREADY_EXISTS : (fileName) => `File with ${fileName} already exists`,
    NO_FILE_FOUND: (fileName) => `No file with ${fileName} filename found`
}

module.exports = responseMessages;