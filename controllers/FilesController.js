const fsp = require('fs/promises');
const extensionHelper = require('./../utils/extensionHelper');
const responseMessages = require('../constants/responseMessages');

class FilesController {
    async createFile(req, res) {
        const fileName = req.body.filename;
        const fileContent = req.body.content;
        const isValidExtension = extensionHelper.isValidExtension(fileName);

        if (!fileName) {
            return res.status(400).send({
                message: responseMessages.NO_FILENAME_PARAM
            });
        }

        if (!fileContent) {
            return res.status(400).send({
                message: responseMessages.NO_CONTENT_PARAM
            });
        }

        if (isValidExtension) {
            let fileHandle;
            try {
                fileHandle = await fsp.open(`${filesDir}/${fileName}`, 'r+');
                res.status(400).send({
                    message: responseMessages.FILE_ALREADY_EXISTS(fileName)
                });
            } catch (e) {
                if (e.code === 'ENOENT') {
                    await fsp.writeFile(`${filesDir}/${fileName}`, fileContent);
                    res.status(200).send({
                        message: responseMessages.FILE_CREATED_SUCCESSFULLY
                    });
                } else {
                    res.status(500).send({
                        message: responseMessages.SERVER_ERROR
                    });
                }
            } finally {
                if (fileHandle) {
                    await fileHandle.close();
                }
            }
        } else {
            res.status(400).send({
                message: responseMessages.INVALID_FILE_FORMAT
            });
        }
    };

    async getFile(req, res) {
        const fileName = req.params.filename;
        const files = await fsp.readdir(filesDir);
        const fullFileName = files.find(file => file.includes(fileName));
        const extension = extensionHelper.getExtension(fullFileName);

        try {
            const fileHandle = await fsp.open(`${filesDir}/${fullFileName}`, 'r');
            const uploadedDate = (await fsp.stat(`${filesDir}/${fullFileName}`)).birthtime;
            try {
                const fileData = await fsp.readFile(fileHandle);
                res.status(200)
                    .json({
                        message: responseMessages.SUCCESS_MESSAGE,
                        filename: fileName,
                        extension: extension,
                        content: fileData.toString(),
                        uploadedDate
                    });
            } catch (e) {
                res.status(500).send({
                    message: responseMessages.SERVER_ERROR
                });
            } finally {
                await fileHandle.close();
            }
        } catch (e) {
            if (e.code === 'ENOENT') {
                res.status(400).send({
                    message: responseMessages.NO_FILE_FOUND(fileName)
                });
            }
        }
    }


    async getFiles(req, res) {
        let files;

        try {
            files = await fsp.readdir(filesDir);
            res.status(200).send({
                message: responseMessages.SUCCESS_MESSAGE,
                files
            })
        } catch (e) {
            res.status(500).send({
                message: responseMessages.SERVER_ERROR
            });
        }
    };
}

module.exports = new FilesController();